# Call-Center

#### Domain graphical representation
![graphical_representations](Capture.PNG)

### User Stories: 
  
#####  Operator   
1. as a Operator I want to have an ability to receive incoming calls
2. as a Operator I want to see user information by incming call number
3. as a Operator I want to choose preconfigured script related to client interest
4. as a Operator I want to have ability to save call result
5. as a Operator I want to call client by generated outgoing call
#####  Administrator
6. as a Administrator I want to have ability to manage script configs
7. as a Administrator I want to have ability generate outgoing call list
8. as a Administrator I want to assign generated outgoing list to particular 
operator group
9. as a Administrator I want to manage operator rights
10. as a Administrator I want to generate call/operator detailed reports





